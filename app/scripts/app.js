'use strict';

angular.module('conceptFocusToolApp', ['ui.router','ngResource', 'ngDialog'])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/navbar.html',
                        controller  : 'NavbarCtrl'
                    },
                    'content': {
                        templateUrl : 'views/homecontent.html'
                    },
                    'footer': {
                        templateUrl : 'views/homefooter.html'
                    }
                }

            })

            .state('app.conceptlist', {   // route for the conceptlist page
                url: 'concepts',
                views: {
                    'content@': {
                        templateUrl : 'views/conceptlist.html',
                        controller  : 'ConceptListController'
                    }
                }
            })

            // route for the cardview
            .state('app.cardview', {
                url: 'concepts/:id/view',
                views: {
                    'content@': {
                        templateUrl : 'views/cardview.html',
                        controller  : 'ConceptsItemViewCtrl'
                   }
                }
            })

            // route for the cardview editing view
            .state('app.conceptsitemedit', {
                url: 'concepts/:id/edit',
                views: {
                    'content@': {
                        templateUrl : 'views/cardedit.html',
                        controller  : 'ConceptsItemEditCtrl'
                   }
                }
            })

            // route for the new concept state
            .state('app.conceptsitemnew', {
                url: 'concepts/concept/new',
                views: {
                    'content@': {
                        templateUrl : 'views/cardedit.html',
                        controller  : 'ConceptsItemNewCtrl'
                   }
                }
            });

        $urlRouterProvider.otherwise('/');
    })
;
