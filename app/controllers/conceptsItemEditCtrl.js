'use strict';

angular.module('conceptFocusToolApp')

.controller('ConceptsItemEditCtrl', ['$scope', '$state', '$stateParams', 'cardFactory', function($scope, $state, $stateParams, cardFactory) {

    $scope.editRequested = true;
    $scope.card = {};

    // get user's data for this item
    cardFactory.get({ id:$stateParams.id })
    .$promise.then(
        function(response) {
            $scope.card = response;
        },
        function(response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        }
    );

    $scope.continueEditing = function() {
        $scope.editRequested = true;
    };

    $scope.requestCancelEditing = function() {
        $scope.editRequested = false;
    };

    $scope.exitEditing = function() {
        $state.go('app.cardview', { id:$stateParams.id });
    };

    $scope.saveConcept = function() {
        cardFactory.update({ id:$stateParams.id }, $scope.card);
        $scope.editRequested = false;
        $state.go('app.cardview', { id:$stateParams.id });
    }
}]);
