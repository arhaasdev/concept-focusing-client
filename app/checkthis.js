<div class="jumbotron" id="searchbar">
    <div class="container">
        <div class="row row-content">
            <div class="col-lg-6">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    <input type="text" class="form-control" placeholder="Search for...">
                </div><!-- /input-group-->
            </div><!-- /.col-lg-6 -->
        </div>
        <div class="row row-content">
            <div class="row">
                <div class="btn-group" role="group" aria-label="order search results">
                    <button type="button" class="btn btn-default">updated</button>
                    <button type="button" class="btn btn-default">created</button>
                    <button type="button" class="btn btn-default">alphanumeric</button>
                    <button type="button" class="btn btn-default">random</button>
                </div>
            </div>
        </div>
    </div><!-- /.col-lg-6 -->
</div><!-- /.row -->






<!-- begin modal -->
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Login </h4>
            </div>
            <div class="modal-body">
                <form class="form-inline">
                    <div class="form-group">
                        <label class="sr-only" for="email">Email address</label>
                        <input type="email" class="form-control" id="email" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="password">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info btn-sm">Sign in</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $('#loginLink').click(function(){
            $('#loginModal').modal('show');
        });
    });
</script>
